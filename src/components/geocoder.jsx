import React, { useState, useCallback  } from "react";
import { makeStyles } from '@material-ui/core/styles';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import { Grid, Paper, IconButton, TextField, Card , InputLabel, Input, InputAdornment, FormControl, List, ListItem, ClickAwayListener, Box } from '@material-ui/core';
import debounce from "lodash/debounce";
//import { LeafletCard } from "./leafletcard";

const useStyles = makeStyles((theme) => ({

	input: {
		padding: '2px 4px',
		margin: '10px 0',
		height: '48px',
		width: 512,
	},
	list: {
		height: '300px'
	},
	iconButton: {
		padding: 10,
		marginTop: '-15px'
	},
	card: {
		padding: '10px 20px',
		marginTop: 20
	},
	paper: {
		maxHeight: 200, 
		width: 520, 
		overflow: 'auto',
		position: 'absolute',
		zIndex: 1
	}
}));

export function Geocoder(props) {
	const classes = useStyles();

	const [visibleMenu, setVisibleMenu] = useState(false);
	const [searchLocation, setSearchLocation] = useState([]);
	const [advertMarker, setAdvertMarker] = useState('');
	const [inputLocation, setInputLocation] = useState("");
	const [selectLocation, setSelectLocation] = useState("");

	

	const makeGeolocationRequest = ()=> {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {			
				const geoLat = position.coords.latitude;
				const geoLon = position.coords.longitude;
					
				geocoderFetch(geoLon+','+geoLat)

				setInputLocation(geoLon+','+geoLat)
				setSelectLocation(geoLon+','+geoLat)
			})
		}
	}

	function geocoderFetch (value) {
		fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=`+props.apiKey+`&format=json&geocode=`+value)
			.then(function(resp) {
			return resp.json();
			})
			.then(json => {
			if (json.response) {
				console.log(json)
				const getCollection = json.response.GeoObjectCollection.featureMember;
				const shallowCopyResult = [...getCollection]
				const getValueResult = Object.values(shallowCopyResult).map(item => item.GeoObject);
				
				setSearchLocation(getValueResult);
				setVisibleMenu(true)
			}
		})
		.catch(error => console.log("api error: ", error));
	}


	const debounceCall = useCallback(// eslint-disable-line 
		debounce((value) => {
		  geocoderFetch(value)
		}, props.time),		
	[]);


	const handleKeyPress = event => {
		let value = event.target.value;
		console.log(value);
		if (value !== "" && value.length >= 2) {
			debounceCall(value);
		} else {
			setSearchLocation([]);
		}
	};



	
	const handleOptionSelect = (data) => {
		setAdvertMarker(data.Point.pos)

		setInputLocation(data.metaDataProperty.GeocoderMetaData.text)
		setSelectLocation(data.metaDataProperty.GeocoderMetaData.text)
	}

	
	const handleFieldChange = (event, element) => {
		let value = event.target.value;
		if (element === 'select') {
			setSelectLocation(value)
		}
		if (element === 'input') {
			setInputLocation(value)
		}
	}



	return (
		<Grid container
			justifyContent="center"
			direction="column"
			alignItems="center"
		>	
			<Card  variant="outlined" className={classes.card}>

				<ClickAwayListener onClickAway={e => setVisibleMenu(false)}>
					<Grid item xs={12} >
					
						<FormControl fullWidth 
							className={classes.input} 
							color="secondary" 
						  >
							  <InputLabel htmlFor="location" >Начните вводить адрес</InputLabel>
							  <Input 
								
								value={selectLocation}
								onKeyUp={event => {
										handleKeyPress(event)
									}	
								}
								onChange={event => {
										handleFieldChange(event, 'select')
									}	
								}
								id="location"
								endAdornment={
								  <InputAdornment position="end">
									<IconButton 
										onClick={makeGeolocationRequest}
										color="secondary" 
										className={classes.iconButton} aria-label="directions">
										<MyLocationIcon />
									</IconButton>
								  </InputAdornment>
								}
							  />
							</FormControl>


				
								

						{visibleMenu &&
							<Paper className={classes.paper}>
								<List style={{maxHeight: '100%', overflow: 'auto'}}>
								   {searchLocation.map((searchResult, index) => (
									<ListItem 
										button component="a"
										key={index} 
										onClick={event => {
											handleOptionSelect(searchResult)
											setVisibleMenu(!visibleMenu)
										}	
									}
									>
									  {searchResult.metaDataProperty.GeocoderMetaData.text} 
									</ListItem >

								  ))}
								</List>
							</Paper>
						}
					
					</Grid>
				</ClickAwayListener>


				<Grid item xs={12} >
					<TextField  
						className={classes.input}  
						color="secondary" 
						value={inputLocation}
						label='Местоположение' 
						onChange={(event) => handleFieldChange(event, 'input')}
					/>	
				</Grid>


				<Grid item xs={12} >

					<p>marker cords: {advertMarker}</p>
					
					

				</Grid>

				<Grid item xs={12} >
				   <Box >
				   		<p>Leflet map place, import and add {'<LeafletCard/>'} here</p>
				   </Box>

				</Grid>

			</Card>
		
		</Grid>


	);
}
