import './App.css';
import { Geocoder } from "./components/geocoder";


function App() {
	//const apiKey = process.env.REACT_APP_YANDEX_API_KEY;
	//for test
	const apiKey = '78788af3-00bd-4b4b-8e24-530b3ef7c4c0'
	const time = 1500;

	return (
		<div className="App">

			<Geocoder apiKey={apiKey} time={time}/>

		</div>
	);
}

export default App;
